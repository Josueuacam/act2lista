package com.mycompany.pila.Josu6;

import java.util.Stack;
import java.util.Scanner;
public class Pilas {
  
    //codigo proporcionado por el maestro Edgar Dzulu
 private static class AC1{
   public AC1 (){
       String cadena = "(123*4)+((12-1)*(1+2)))";
        Stack  pila   = new Stack();
        boolean flag  = true;
        char[] acad   = cadena.toCharArray();
        for (char c : acad) {
            if (c == '('){
                pila.push(c);
                System.out.println("Insertando = "+c);
            }
            if (c == ')'){
                if (!pila.empty()){
                    pila.pop();
                    System.out.println("Extrayendo = "+c);
                }else{
                    flag = false;
                }
                    
            }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if (pila.size()== 0 && flag){
            System.out.println("Expresion Valida");            
        }else{
            System.out.println("Expresion Invalida");
        }
   }
  }
  // mi codigo guia del ejercicio anterior
  private static class AC2{
   public AC2(){
       String Html ="<b><i>Hola ISC</i></b";
       Stack pila1 = new Stack();
       boolean ntv = true;
       char[] jos   = Html.toCharArray();
       for (char j : jos) {
            if (j == '<'){
                pila1.push(j);
                System.out.println("Insertando = "+j);
            }
            if (j == '>'){
                if (!pila1.empty()){
                    pila1.pop();
                    System.out.println("Extrayendo = "+j);
                }else{
                    ntv = false;
                }
                    
            }
        }
       System.out.println(pila1.size());
        System.out.println(ntv);
        if (pila1.size()== 0 && ntv){
            System.out.println("Expresion Valida");            
        }else{
            System.out.println("Expresion Invalida");
        }
   }
  }
  
  //no lo logre concluir seguire intentando
    public static void main(String[] args) {
        System.out.println("ACTIVIDAD 1");
        AC1 ejecucion1 = new AC1();
        System.out.println("--------------------------------------------");
        System.out.println("ACTIVIDAD 2");
        AC2 ejecucion2 = new AC2();
        System.out.println("---------------------------------------------");
        System.out.println("ACTIVIDAD 3");
        Scanner entrada = new Scanner(System.in);
        String respuesta = "S";
        String cadena="";
        while (respuesta.equalsIgnoreCase("S")){
            System.out.println ("¿Qué cadena desea analizar?");
            cadena=entrada.nextLine();
            String textoPorPantalla="";
            char[] array = {'a','e','i','o','u'};
            for (int i=0; i<array.length; i++) {
                switch (analizarVocal(cadena,array[i])){
                    case 1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es par. "; break;
                    case -1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es impar. "; break;
                    case 0: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es cero. "; break;
                }
            }
                System.out.println (textoPorPantalla);
                System.out.print ("¿Desea analizar otra cadena? (S/N) ");
                respuesta = entrada.nextLine();  
                
        }
    }
      public static int analizarVocal (String cadena, char vocalParaAnalizar) {
        Stack<String> pila = new Stack<String>(); char v=vocalParaAnalizar; String vocal=String.valueOf(vocalParaAnalizar);
        int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (Character.toLowerCase(cadena.charAt(i))==v&&pila.empty()) {pila.push(vocal); auxiliar++;}
            else if (Character.toLowerCase(cadena.charAt(i))==v&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}     
    }
    
    
}

