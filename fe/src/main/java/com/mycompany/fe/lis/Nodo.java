/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fe.lis;

/**
 *
 * @author pc
 */
public class Nodo {
    public String dato;    // Valor a almacenar
    public Nodo siguiente;  // Puntero, del mismo tipo de la clase
    
    // Constructor para insertar el dato
    public Nodo(String d){
        this.dato = d;
    }
    // Constructor para insertar al inicio de la lista
    public Nodo(String d, Nodo n){
        dato = d;
        siguiente = n;
    }
}
